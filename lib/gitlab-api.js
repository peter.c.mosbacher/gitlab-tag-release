'use strict';

const axios = require('axios');
const validate = require('./data-validators');

const gitlabAPI = (token, url) => {
    validate.hasValue(token, 'Invalid GitLab API access token');
    validate.isGitlabApiUrl(url);

    axios.defaults.baseURL = `${url}`;
    axios.defaults.headers.common['PRIVATE-TOKEN'] = token;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios.defaults.headers.put['Content-Type'] = 'application/json';

    const getTagData = (projectID, tag) => {
        validate.hasValue(projectID, 'Invalid projectID');
        validate.hasValue(tag, 'Invalid tag');

        const getTagUrl = `/projects/${projectID}/repository/tags/${tag}`;
        console.log(`Retrieving data for tag ${tag}`);
        return axios.get(getTagUrl);
    };

    const postNewRelease = (projectID, release) => {
        validate.hasValue(projectID, 'Invalid projectID');
        validate.isRelease(release);

        const postReleaseUrl = `/projects/${projectID}/releases/`;
        const data = { tag_name: release.tag, name: release.title, description: release.releaseNotes };
        console.log(`Creating release - ${release.title}`);
        return axios.post(postReleaseUrl, data);
    };

    const putReleaseUpdate = (projectID, release) => {
        validate.hasValue(projectID, 'Invalid projectID');
        validate.isRelease(release);

        const putReleaseUrl = `/projects/${projectID}/releases/${release.tag}`;
        const data = { name: release.title, description: release.releaseNotes };
        console.log(`Updating release - ${release.title}`);
        return axios.put(putReleaseUrl, data);
    };

    return {
        getTagData,
        postNewRelease,
        putReleaseUpdate
    };
};

module.exports = gitlabAPI;
