'use strict';

const hasValue = (data, msg) => {
    if (!data) {
        throw new TypeError(msg);
    }
};

const isGitlabApiUrl = (url) => {
    if (!url || !url.match(/https?:\/\/.*\/api.*/i)) {
        throw new TypeError('Invalid GitLab API URL');
    }
};

const isRelease = (release) => {
    if (!release.tag || ! release.title || !release.releaseNotes) {
        throw new TypeError('Invalid release - must contain at least a tag, title, and releaseNotes');
    }
};

module.exports.hasValue = hasValue;
module.exports.isGitlabApiUrl = isGitlabApiUrl;
module.exports.isRelease = isRelease;
