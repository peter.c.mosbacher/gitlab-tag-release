GitLab Tag Release
=========================

## About GitLab Tag Release
GitLab Tag Release is a Node.js application that creates a new release via the GitLab API for any new tag using the tag message as the release name and default set of release notes.  If a release exists for the tag, the release name is set to the tag message.