'use strict';

const fs = require('fs');
const path = require('path');
const validate = require('./data-validators');

const findChangelog = (location) => {
    validate.hasValue(location, 'Invalid location');

    const allChangelogsRegex = /changelog/i;
    const primaryChangelogRegex = /changelog.md/i;
    const files = fs.readdirSync(location);
    const changelogs = files.filter(f => f.match(allChangelogsRegex));
    let changelog;
    
    if (changelogs.length === 0) {
        console.error(`No changelog found in directory '${location}'`);
    }
    else if (changelogs.length === 1) {
        changelog = changelogs[0];
        console.log(`Changelog found: ${changelog}`);
    }
    else {
        const primaryChangelogs = files.filter(f => f.match(primaryChangelogRegex));
        if (primaryChangelogs.length === 1) {
            changelog = primaryChangelogs[0];
        }
        else {
            changelog = changelogs[0];
        }
        console.error(`Multiple changelogs found, using ${changelog}`);
    }

    if (changelog) {
        return path.join(location, changelog);
    }
    else {
        return undefined;
    }
};

module.exports = findChangelog;
