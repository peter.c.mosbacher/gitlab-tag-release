'use strict';

const fs = require('fs');
const validate = require('./data-validators');

const getReleaseDataFromChangelog = (changelog, tag) => {
    validate.hasValue(changelog, 'Invalid changelog location');
    validate.hasValue(tag, 'Invalid commit tag');

    const searchTag = tag.startsWith('v') ? tag.slice(1) : tag;
    const re = new RegExp(`(#{2,} )(.*${searchTag}.*)$`);
    let releaseFound = false;
    let releaseDelimiter = '##';
    let result = { title: '', releaseNotes: ''};

    let fileLines = fs.readFileSync(changelog, 'utf-8').split(/\r?\n/);
    for (let line of fileLines) {
        if (releaseFound) {
            if (line.startsWith(releaseDelimiter)) {
                break;
            }
            else {
                result.releaseNotes += `${line}\n`;
            }
        }
        else {
            let match = line.match(re);
            if (match) {
                releaseFound = true;
                releaseDelimiter = match[1];
                result.title = match[2];
            }
        }
    }

    if (releaseFound) {
        return result;
    }
    else {
        return undefined;
    }
};

module.exports = getReleaseDataFromChangelog;
