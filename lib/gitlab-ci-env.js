'use strict';

module.exports = {
    isCI: process.env.CI,
    CI: {
        apiUrl: process.env.CI_API_V4_URL,
        commit: {
            tag: process.env.CI_COMMIT_TAG
        },
        project: {
            ID: process.env.CI_PROJECT_ID
        }
    }
};
