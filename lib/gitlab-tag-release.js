'use strict';

const env = require('./gitlab-ci-env');
const gitlabApi = require('./gitlab-api');
const findChangelog = require('./find-changelog');
const changelogRelease = require('./changelog-release');
const validate = require('./data-validators');

const getConfig = () => {
    const config = {
        token: process.env.API_TOKEN,
        apiUrl: env.CI.apiUrl,
        projectID: env.CI.project.ID,
        tag: env.CI.commit.tag
    };
    return config;
};

const validateConfig = (config) => {
    validate.hasValue(config.token, 'Invalid GitLab API access token');
    validate.isGitlabApiUrl(config.apiUrl);
    validate.hasValue(config.projectID, 'Invalid project ID. Must be a numeric value or the URL-encoded path of the project.');
    validate.hasValue(config.tag, 'Invalid commit tag.  Ensure job is only run against tagged commits.');
};

const compileReleaseData = (releaseData, tagData) => {
    const releaseDescriptionDefault = '### Changelog\n\n- ADD DETAILS\n';    
    let result;
    
    if (releaseData) {
        result = releaseData;
    }
    else {
        result = {
            title: tagData.message ? tagData.message : tagData.name, 
            releaseNotes: releaseDescriptionDefault
        };
    }
    result.hasRelease = tagData.release ? true : false;
    result.tag = tagData.name;
    return result;
};

const completeRelease = () => {
    console.log('Release update complete');
};

const handleError = (message) => {
    console.error(message);
    process.exit(1);
};

const processRelease = async() => {
    const config = getConfig();
    validateConfig(config);

    const gitlab = gitlabApi(config.token, config.apiUrl);
    const tagData = await gitlab.getTagData(config.projectID, config.tag)
        .catch((err) => handleError(`Error retrieving tag data - ${err.message}`));

    const changelog = findChangelog('./');
    let releaseFromChangelog;
    if (changelog) {
        releaseFromChangelog = changelogRelease(changelog, config.tag);
    }
    const releaseData = compileReleaseData(releaseFromChangelog, tagData.data);

    if (releaseData.hasRelease) {
        await gitlab.putReleaseUpdate(config.projectID, releaseData)
            .catch((err) => handleError(`Error updating release - ${err.message}`));
    }
    else {
        await gitlab.postNewRelease(config.projectID, releaseData)
            .catch((err) => handleError(`Error creating release - ${err.message}`));
    }
    completeRelease();
};

module.exports.process = processRelease;
